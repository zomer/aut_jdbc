package helper;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import java.util.Map;

public class JtwigTemplateHelper {

    public static void render(String templateName, Map<String, Object> data, ShowStrategy showStrategy) {

        JtwigTemplate template = JtwigTemplate.fileTemplate(PropertiesCache.getInstance().getProperty("path") + templateName + ".twig");
        JtwigModel model = JtwigModel.newModel(data);

        showStrategy.show(template, model);
    }

}
