package controller;

import helper.ConsoleView;

import java.sql.SQLException;

import static helper.JtwigTemplateHelper.render;
import static helper.WorldHelper.getCountriesWithLanguages;
import static helper.WorldHelper.getSorterCountries;

public class WorldController implements IController {

    public void showCountries(String url) throws SQLException {
        render("countries", getSorterCountries(url), new ConsoleView());
    }

    public void showCountriesWithLanguages(String url) throws SQLException {
        render("countriesWithLanguages", getCountriesWithLanguages(url), new ConsoleView());
    }
}
