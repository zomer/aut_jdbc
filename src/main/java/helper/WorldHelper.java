package helper;

import model.Country;
import model.Countrylanguage;

import java.sql.*;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by marg on 02.12.16.
 */
public class WorldHelper {

    public static Map<String, Object> getSorterCountries(String url) throws SQLException {
        Connection connection = DriverManager.getConnection(url);
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT NAME from COUNTRY ORDER BY NAME");
        HashMap data = new HashMap();

        List<Country> countries = new ArrayList<>();
        while (rs.next()) {
            Country.Builder countryBuilder = new Country.Builder();
            countries.add(countryBuilder.name(rs.getString("NAME")).build());
        }

        connection.close();

        data.put("countries", countries);

        return data;
    }


    public static Map<String, Object> getCountriesWithLanguages(String url) throws SQLException {
        Connection connection = DriverManager.getConnection(url);
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * from COUNTRY AS c INNER JOIN  countrylanguage AS l  ON c.code = l.countrycode ORDER BY NAME");
        HashMap data = new HashMap();

        Set<Country> countries = new LinkedHashSet<>();

        while (rs.next()) {

            String name = rs.getString("NAME");
            String langname = rs.getString("LANGUAGE");


            boolean isCountryPresent = countries.stream()
                    .filter(e -> e.getName().equals(name))
                    .findAny()
                    .isPresent();

            if (isCountryPresent) {
                countries.forEach(e -> {
                    if (e.getName().equals(name)) {
                        e.addCountrylanguage(new Countrylanguage(langname));
                    }
                });
            } else {
                Country.Builder countryBuilder = new Country.Builder();
                List<Countrylanguage> countrylanguages = new ArrayList<>();
                countrylanguages.add(new Countrylanguage(langname));
                countries.add(countryBuilder.name(name).countrylanguage(countrylanguages).build());
            }
        }

        connection.close();
        data.put("countries", countries);

        return data;
    }
}
