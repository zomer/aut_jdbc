package helper;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import java.util.Map;

/**
 * Created by marg on 05.12.16.
 */
public interface ShowStrategy {
    void show(JtwigTemplate template, JtwigModel model);
}
