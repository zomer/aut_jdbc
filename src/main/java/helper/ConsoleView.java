package helper;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static java.lang.System.currentTimeMillis;

/**
 * Created by marg on 05.12.16.
 */
public class ConsoleView implements ShowStrategy {

    @Override
    public void show(JtwigTemplate template, JtwigModel model) {
        template.render(model,  new DataOutputStream(System.out));
    }
}
