package helper;

import model.*;

import java.sql.*;
import java.util.*;

public class DumpHelper {

    public static Map getData(String databaseUrl) {
        try {
            HashMap data = new HashMap();
            data.put("base", getDataBase(databaseUrl));
            return data;
        } catch (SQLException | ArrayIndexOutOfBoundsException e) {
            System.out.print("Error in DB getData() method " + e.getMessage());
            return new HashMap();
        }
    }

    private static DataBase getDataBase(String databaseUrl) throws SQLException {
        Connection connection = DriverManager.getConnection(databaseUrl);
        DatabaseMetaData metaData = connection.getMetaData();
        DataBase dataBase = new DataBase();
        dataBase.setName(getDataBaseName(metaData) + System.currentTimeMillis());
        dataBase.setTables(getTables(metaData));
        connection.close();

        return dataBase;
    }

    private static String getDataBaseName(DatabaseMetaData data) throws SQLException {
        return Optional.of(data.getURL()).map(url -> url.substring(url.lastIndexOf('/') + 1).split("\\?")[0]).get();
    }

    private static List<Table> getTables(DatabaseMetaData data) throws SQLException {
        ResultSet metaTables = data.getTables(null, null, null, new String[]{"TABLE"});
        List<Table> tables = new ArrayList();

        while (metaTables.next()) {
            Table table = new Table();
            String tableName = metaTables.getString("TABLE_NAME");
            table.setName(tableName);
            table.setFields(getFields(data, tableName));
            table.setPrimaryKeys(getPrimaryKeys(data, tableName));
            table.setForeignKeys(getForeignKey(data, tableName));
            tables.add(table);
        }

        return tables;
    }

    private static List<ForeignKey> getForeignKey(DatabaseMetaData data, String tableName) throws SQLException {
        List<ForeignKey> foreignKeys = new ArrayList<>();
        ResultSet keys = data.getImportedKeys(null, null, tableName);

        while (keys.next()) {
            ForeignKey foreignKey = new ForeignKey();
            foreignKey.setFkTableName(keys.getString("FKTABLE_NAME"));
            foreignKey.setFkColumnName(keys.getString("FKCOLUMN_NAME"));
            foreignKey.setPkTableName(keys.getString("PKTABLE_NAME"));
            foreignKey.setPkColumnName(keys.getString("PKCOLUMN_NAME"));
        }

        return foreignKeys;
    }

    private static List<PrimaryKey> getPrimaryKeys(DatabaseMetaData data, String tableName) throws SQLException {
        List<PrimaryKey> primaryKeys = new ArrayList<>();
        ResultSet keys = data.getPrimaryKeys(null, null, tableName);

        while (keys.next()) {
            PrimaryKey primaryKey = new PrimaryKey();
            primaryKey.setColumnName(keys.getString("COLUMN_NAME"));
            primaryKeys.add(primaryKey);
        }

        return primaryKeys;
    }

    private static List<Field> getFields(DatabaseMetaData metadata, String tableName) throws SQLException {
        List<Field> fields = new ArrayList<>();
        ResultSet column = metadata.getColumns(null, null, tableName, null);

        while (column.next()) {
            Field field = new Field();
            field.setName(column.getString("COLUMN_NAME"));
            field.setRequared(column.getInt("NULLABLE"));
            String type = column.getString("TYPE_NAME");
            if (type.equals("serial") || type.equals("int4")) {
                field.setType("integer");
            } else {
                field.setType(type);
            }
            fields.add(field);
        }

        return fields;
    }
}
