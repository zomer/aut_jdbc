package helper;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

import static java.lang.System.currentTimeMillis;

/**
 * Created by marg on 05.12.16.
 */
public class FileView implements ShowStrategy {

    @Override
    public void show(JtwigTemplate template, JtwigModel model) {
        try {
            template.render(model, new FileOutputStream(new File("dump-" + currentTimeMillis() + ".sql")));
        } catch (FileNotFoundException e) {
            System.out.print("Error in SqlFile render() method " + e.getMessage());
        }
    }
}
