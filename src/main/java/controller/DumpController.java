package controller;

import helper.FileView;

import static helper.DumpHelper.getData;
import static helper.JtwigTemplateHelper.render;

public class DumpController implements IController {

    public void dump(String url) {
        render("dump", getData(url), new FileView());
    }
}


