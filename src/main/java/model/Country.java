package model;

import java.util.List;

public class Country {
    String name;

    List<Countrylanguage> countrylanguages;

    private Country(Builder builder) {
        name = builder.name;
        countrylanguages = builder.countrylanguages;
    }


    public static final class Builder {
        private String name;
        private List<Countrylanguage> countrylanguages;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder countrylanguage(List<Countrylanguage> val) {
            countrylanguages = val;
            return this;
        }

        public Country build() {
            return new Country(this);
        }
    }

    public String getName() {
        return name;
    }

    public void addCountrylanguage(Countrylanguage countrylanguage) {
        countrylanguages.add(countrylanguage);
    }

    public List<Countrylanguage> getCountrylanguages() {
        return countrylanguages;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountrylanguage(List<Countrylanguage> countrylanguages) {
        this.countrylanguages = countrylanguages;
    }
}
