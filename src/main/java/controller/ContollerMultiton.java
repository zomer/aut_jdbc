package controller;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marg on 02.12.16.
 */
public class ContollerMultiton {

    private static final Map<Object, IController> instances = new HashMap<Object, IController>();

    private ContollerMultiton() {
        // no explicit implementation
    }

    public static synchronized IController getController(Class<? extends IController> clazz) throws IllegalAccessException, InstantiationException {
        String key = clazz.getCanonicalName();
        // Our "per key" singleton
        IController instance = instances.get(key);
        if (instance == null) {
            // Lazily create instance
            instance = clazz.newInstance();
            // Add it to map
            instances.put(key, instance);
        }
        return instance;
    }
}
