package model;

/**
 * Created by marg on 27.10.16.
 */
public class PrimaryKey {

    private String columnName;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
}
