package model;

public class Field {

    private String name;
    private String type;
    private int requared;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRequared() {
        return requared;
    }

    public void setRequared(int requared) {
        this.requared = requared;
    }
}
