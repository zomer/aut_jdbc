import java.sql.SQLException;

public class Main {

    /**
     * My test url "jdbc:postgresql://localhost:5432/world?username=marg&password=123"
     */

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, SQLException {

    /**
     * PART 1 dump
     * new DumpController().dump(args[0]);
     */

    /**
     * PART 2.1 Показать страны по алфавиту
     */
//    Optional.of(getController(WorldController.class))
//            .filter(WorldController.class::isInstance)
//            .map(WorldController.class::cast)
//            .get().showCountries(args[0]);

    /**
     * The ability to use a controller
     *
     *
     * WorldController worldController = (WorldController) getController(WorldController.class);
     * worldController.showCountries(args[0]);
     *
     * WorldController worldController = new WorldController();
     * worldController.showCountries(args[0]);
     *
     *  new WorldController().showCountries(args[0]);
     *
     * ((WorldController) getController(WorldController.class)).showCountries(args[0]);
     */

    /**
     * PART 2.2 Вывести весь список стран с указанием языка
     */
//    Optional.of(getController(WorldController.class))
//            .filter(WorldController.class::isInstance)
//            .map(WorldController.class::cast)
//            .get().showCountriesWithLanguages(args[0]);

     /**
      * PART 2.3 Возможность изменять столицу страны
      */
//    Optional.of(getController(WorldController.class))
//            .filter(WorldController.class::isInstance)
//            .map(WorldController.class::cast)
//            .get().showCountriesWithLanguages(args[0]);
    }
}
